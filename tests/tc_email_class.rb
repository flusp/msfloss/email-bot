require './src/email.rb'

require 'test/unit'
require 'gmail'

class TestEmailClass < Test::Unit::TestCase
  def setup
    @email = Email.new
  end

  def test_crete_email
    assert(@email.empty?)
    assert_equal([], @email.messages)
  end

  def test_new_message
    message = 'this is a message'
    @email.add_message(message)
    assert_equal(false, @email.empty?)
    assert_equal([message], @email.messages)
  end

  def test_clear_messages
    message = 'this is a message'
    @email.add_message(message)
    assert_equal(false, @email.empty?)
    @email.clear_messages
    assert_equal(true, @email.empty?)
  end
end
