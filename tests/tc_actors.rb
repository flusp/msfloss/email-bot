require 'test/unit'
require 'gmail'

class TestGmail < Test::Unit::TestCase
  def setup
    @gmail = Gmail.connect(ENV['MS_FLOSS_USERNAME'], ENV['MS_FLOSS_PASSWORD'])
  end

  def test_login
    assert(@gmail.logged_in?)
  end

  def test_wrong_user
    gmail = Gmail.connect('wrong_user', ENV['MS_FLOSS_PASSWORD'])
    assert_equal(false, gmail.logged_in?)
  end

  def test_wrong_password
    gmail = Gmail.connect(ENV['MS_FLOSS_USERNAME'], 'wrong_pass')
    assert_equal(false, gmail.logged_in?)
  end

  def test_count_email
    assert(@gmail.inbox.count.positive?)
  end

  def test_get_email
    email = @gmail.inbox.emails
    assert_not_nil(email)
  end
end
