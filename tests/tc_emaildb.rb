require 'test/unit'
require 'pg'

class TestPG < Test::Unit::TestCase
  def setup
    @con = PG.connect
  end

  def test_login
    assert(@con)
    @con.close
  end

  def test_wrong_user
    con = PG.connect(dbname: ENV['PGDATABASE'], host: ENV['PGHOST'],
                     port: ENV['PGPORT'], user: 'wrong_user',
                     password: ENV['PGPASSWORD'])
    assert_nil(con)
  end

  def test_wrong_password
    con = PG.connect(dbname: ENV['PGDATABASE'], host: ENV['PGHOST'],
                     port: ENV['PGPORT'], user: ENV['PGUSER'],
                     password: 'wrong_pass')
    assert_nil(con)
  end

  def test_count_email
    result = @con.exec 'SELECT count(*) FROM emails;'
    assert_not_nil(result)
  end

  def test_count_lists
    result = @con.exec 'SELECT count(*) FROM lists;'
    assert_not_nil(result)
  end
end
