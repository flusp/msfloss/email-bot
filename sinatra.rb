# E-mail bot API prototype
# To run this API prototype locally exec 'ruby sinatra.rb' and access http://localhost:4567
# For a mock, try:
# http://localhost:4567/mailbot?from=Fulano&to=Ciclano&subject=PATCH&since_day=20/02/2019&until_day=07/05/2019
require 'sinatra'

load 'src/emaildb.rb'

get '/' do
  'Hello world!'
end

def build_response(params)
  'searching emails: ' \
             "<br/> from: #{params['from']}," \
             "<br/>to: #{params['to']}," \
             "<br/>with subject: #{params['subject']}," \
             "<br/>since day: #{params['since_day']}," \
             "<br/>until_day: #{params['until_day']}," \
             '<br/>found emails: '
end

def create_conditions(params)
  conditions = {}
  conditions['dateini'] = params['since_day']
  conditions['datefinal'] = params['until_day']
  conditions['sender'] = params['from']
  conditions['count'] = params['count']
  conditions
end

def print_row(row)
  puts "#{row['email_id']} #{row['sender']} #{row['recipient']} \
         #{row['date']} #{row['message']} #{row['subject']} \
         #{row['message_id']} #{row['in_reply_to']} #{row['action']} \
         #{row['subsystem']} #{row['subject_original']}"
end

def row_information(row)
  "<br/>email_id: #{row['email_id']}" \
       "<br/>sender: #{row['sender']}" \
       "<br/>recipient: #{row['recipient']}" \
       "<br/>date: #{row['date']}" \
       "<br/>message: #{row['message']}" \
       "<br/>subject: #{row['subject']}" \
       "<br/>message_id: #{row['message_id']}" \
       "<br/>in_reply_to: #{row['action']}" \
       "<br/>subsystem: #{row['subsystem']}" \
       "<br/>subject_original: #{row['subject_original']}<br/>"
end

# def fetch_data(conn, jjjjjj

get '/mailbot' do
  # matches "GET /mailbot?<param>=<arg>&<param>=<arg>"
  # uses title and author variables; query is optional to the /posts route
  # http://localhost:4567/mailbot?from=Fulano&to=Ciclano&subject=PATCH&since_day=20/02/2019&until_day=07/05/2019
  # To run sinatra on the server run
  # ruby sinatra.rb -o 206.189.209.87
  # Then access, for example:
  # http://206.189.209.87:4567/
  # http://206.189.209.87:4567//mailbot?from=marcelo.schmitt1@gmail.com&since_day=2019/02/20&until_day=2019/05/30

  # Connect to DB
  mailrepository = MailRepository.new(
    'dbname=developer '\
    'host=localhost '\
    'port=5432 '\
    'user=developer '\
    'password=fabio_kon'
  )
  con = mailrepository.opendb

  conditions = create_conditions(params)
  selectstr = mailrepository.selectstr(conditions)
  puts selectstr

  response = build_response(params)

  rs = con.exec selectstr
  rs.each do |row|
    print_row(row)
    respose << row_information(row)
  end

  mailrepository.closedb
  response
end
