# Email Bot

## Goal

Our goal is to create an email bot, which is a program that listen an email and
check if there are unread messages. Once a message arrives, we will read it and
save it in a database.

## How to Run

To run the bot you need to use a gem called `rake`.
The command to run the email bot is:

```
rake app:run
```

If you want to test the project you can even run:

```
rake test
```

## Development Process

### Set Environment Variables

To be able to run our codebase you need to export the email and database 
login information. For the database, the port used is usually 5432. 

Email login information:

```
export MS_FLOSS_USERNAME=<username>
export MS_FLOSS_PASSWORD=<password>
```
Database login information:

```
export PGDATABASE=<databasename>
export PGHOST=<host>
export PGPORT=<port>
export PGUSER=<username>
export PGPASSWORD=<password>

```

### Quality Code

We should be able to maintain a readable and with high quality code. For this
reason, before pushing something make sure that you run this command:

```
rubocop -a
```

If you don't have `rubocop` on your computer, run:

```
gem install rubocop
```

## Meeting History

* Meeting 04/11/2019
- https://pad.riseup.net/p/Pauta11-04
