# app.rb
require 'sinatra'
require 'sinatra/base'
require 'sinatra/activerecord'
require 'sinatra/namespace'

class Email < ActiveRecord::Base
  scope :sender, lambda { |sender|
    where('sender = ?', sender) if sender.present?
  }
  scope :recipient, lambda { |recipient|
    where('recipient = ?', recipient) if recipient.present?
  }
  scope :message_contains, lambda { |message|
    where('message LIKE ?', '%' << message << '%') if message.present?
  }
  scope :subject, lambda { |subject|
    where('subject = ?', subject) if subject.present?
  }
  scope :subject_contains, lambda { |subject|
    where('subject LIKE ?', '%' << subject << '%') if subject.present?
  }
  scope :message_id, lambda { |message_id|
    where('message_id = ?', message_id) if message_id.present?
  }
  scope :in_reply_to, lambda { |in_reply_to|
    where('in_reply_to = ?', in_reply_to) if in_reply_to.present?
  }
  scope :action, lambda { |action|
    where('action = ?', action) if action.present?
  }
  scope :subsystem, lambda { |subsystem|
    where('subsystem = ?', subsystem) if subsystem.present?
  }
  scope :subject_original, lambda { |subject_original|
    where('subject_original = ?', subject_original) if subject_original.present?
  }
  scope :subject_original_contains, lambda { |subject_original|
    if subject_original.present?
      where(
        'subject_original LIKE ?', '%' << subject_original << '%'
      )
    end
  }
  scope :start_date, lambda { |start_date|
    where('date >= ?', start_date) if start_date.present?
  }
  scope :end_date, lambda { |end_date|
    where('date <= ?', end_date) if end_date.present?
  }
end

class Lists < ActiveRecord::Base
end

class EmailApi < Sinatra::Base
  register Sinatra::Namespace

  get '/' do
    erb :index
  end

  namespace '/api/v1' do
    before do
      content_type 'application/json'
    end

    get '/' do
      erb :index
    end

    get '/subjects' do
      Email.pluck(:subject_original).uniq.to_json
    end

    post '/subject/times' do
      subject = JSON.parse(request.body.read)['subject']
      Email.where(['subject = ? OR subject_original = ?', subject, subject])
           .select('MIN(subject) as subject,
				 		  extract(epoch from NOW() - MIN(date)) * 1000 as time_from_start,
				 		  extract(epoch from NOW() - MAX(date)) * 1000 as time_from_last_reply,
				 		  extract(epoch from MAX(date) - MIN(date)) * 1000 as thread_duration')
           .to_json(except: :email_id)
    end

    get '/lists' do
      Lists.all.order(name: :desc).to_json
    end

    post '/lists/subscribe' do
      name = JSON.parse(request.body.read)['name']
      @list = Lists.find_or_create_by(name: name)
      @list.unsubscribe = false
      @list.save
      @list.to_json
    end

    put '/lists/unsubscribe/:list_id' do
      @list = Lists.find(params[:list_id])
      @list.unsubscribe = true
      @list.save
      @list.to_json
    end

    get '/emails' do
      Email.all.order(date: :desc).to_json
    end

    get '/emails/count' do
      { count: Email.all.size }.to_json
    end

    get '/emails/id/:email_id' do
      Email.find(params[:email_id]).to_json
    end

    get '/email-lists' do
      Email.all.group_by(&:recipient).to_json
    end

    get '/email-lists/id/:list_id' do
      emails = Email.all.group_by(&:recipient)
      recipient = Lists.find(params[:list_id]).recipient

      emails[recipient].to_json
    end

    get '/emails/query' do
      Email.sender(params[:sender])
           .recipient(params[:recipient])
           .message_contains(params[:message_contains])
           .subject(params[:subject])
           .subject_contains(params[:subject_contains])
           .message_id(params[:message_id])
           .in_reply_to(params[:in_reply_to])
           .action(params[:action])
           .subsystem(params[:subsystem])
           .subject_original(params[:subject_original])
           .subject_original_contains(params[:subject_original_contains])
           .start_date(params[:start_date])
           .end_date(params[:end_date])
           .to_json
    end
  end
end
