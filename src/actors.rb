require 'gmail'

# Acessa o Gmail, conecta com um usuario especifico inscrito na lista IIO
# pega os emails e monta um lista com os e-mails nao lidos para devolver
class Listener
  attr_reader :email

  def initialize
    @email = Email.new
  end

  def listen
    gmail = Gmail.connect(ENV['MS_FLOSS_USERNAME'], ENV['MS_FLOSS_PASSWORD'])
    mails = gmail.inbox.emails(:unread)
    mails.each do |msg|
      @email.add_message(Message.new(msg.message))
      # change status to read
      msg.read!
    end
    gmail.logout
  end
end

# Chama a classe Listener
class EmailListBot
  attr_reader :listener

  def initialize
    @listener = Listener.new
  end
end

# Gerencia subscricao em listas de discussao
class Maillist
  attr_reader :signed_of

  def initialize
    @signed_of = []
  end

  def add_subscribe_email(subscribe_email)
    @signed_of << subscribe_email
    @signed_of.uniq!
  end

  # Send e-mail
  def send(mail_to, mail_body, mail_subject = '')
    gmail = Gmail.connect(ENV['MS_FLOSS_USERNAME'], ENV['MS_FLOSS_PASSWORD'])
    msg = gmail.compose do
      to mail_to
      subject mail_subject
      body mail_body
    end
    msg.deliver!
    gmail.logout
  end

  # Subscribe to listname
  def subscribe(listname, email_to)
    @signed_of << email_to
    @signed_of.uniq!
    send(email_to, 'subscribe ' + listname)
  end

  # Unsubscribe to listname
  def unsubscribe(listname, email_to)
    send(email_to, 'unsubscribe ' + listname)
  end
end
