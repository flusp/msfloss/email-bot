#!/usr/bin/env ruby
load 'src/email.rb'
load 'src/actors.rb'
load 'src/emaildb.rb'

# Delay time to check new messages
$delay_messages = 10

bot = EmailListBot.new
listener = bot.listener
mails = listener.email
maillist = Maillist.new
mailrepository = MailRepository.new(maillist)

def print_mail(mail)
  puts "#{mail.is_reply} - #{mail.date} - #{mail.author.email} - " \
       "#{mail.subject.full_name} - #{mail.subject.patch} - " \
       "#{mail.subject.subsystem}\n#{mail.content.raw_content}"
end

loop do
  mailrepository.verify_subscription
  mailrepository.verify_unsubscription
  listener.listen

  unless mails.empty?
    msgs = mails.messages
    mailrepository.insert(msgs)
    mails.clear_messages
  end

  sleep $delay_messages
end
