class Email
  attr_reader :messages
  # this class should group all mails from a same list and subject, which is
  # from just one Patch optimize for mail searches
  def initialize
    @messages = []
  end

  def add_message(message)
    @messages << message
  end

  def clear_messages
    @messages = []
  end

  def search_message(key)
    # get emailList and search for a certain string
  end

  def search_patches(key)
    # search for specific patch emails
  end

  def empty?
    @messages.empty?
  end
end

class MessageDB
  attr_reader :date, :author, :content, :subject, :maillist, :message_id,
              :in_reply_to, :count, :email_id, :is_reply

  def initialize(msg)
    @count = msg['number']
    @email_id = msg['email_id']
    @date = msg['date']
    @is_reply = (msg['in_reply_to'] != '')
    @message_id = msg['message_id']
    @in_reply_to = msg['in_reply_to']
    @maillist = msg['recipient']
    @subject = Subject.new(msg['subject'], msg['action'], msg['subsystem'])
    @author = Author.new(msg['sender'], 'intense collaboration', 'flooder')
    @content = Content.new(msg['message'], '')
  end
end

class Message
  attr_reader :date, :author, :content, :subject, :maillist, :message_id,
              :in_reply_to, :is_reply

  def convertdate(date)
    # Use GMT (UTC) timezone
    @newdate = date.new_offset('0')
    @newdate.strftime('%F %T')
  end

  def patch_from(subject)
    patch_part = subject.gsub(/Re:/i, '').gsub(/\]\s*\[/, ' ')
                        .slice(/\[.*\]/i)
    patch = patch_part[1..-2] if patch_part
    patch
  end

  def subsystem_from(subject)
    subsystem_part = subject.gsub(/Re:/i, '').gsub(/\]\s*\[/, ' ')
                            .slice(/\].*\:/)
    subsystem = subsystem_part[1..-2] if subsystem_part
    subsystem
  end

  def initialize(msg)
    @date = convertdate(msg.date)
    @is_reply = !msg.in_reply_to.nil?
    @message_id = msg.message_id
    @in_reply_to = msg.in_reply_to
    @maillist = msg.sender
    sub = msg.subject.to_s
    @subject = Subject.new(sub, patch_from(sub), subsystem_from(sub))
    @author = Author.new(msg.from.to_s[2..-3], 'intense collaboration',
                         'flooder')
    @content = Content.new((msg.text_part ? msg.text_part.body : msg.body), '')
  end
end

class Subject
  attr_reader :full_name, :patch, :subsystem

  def initialize(full_name, patch, subsystem)
    @full_name = full_name
    @subsystem = subsystem
    @patch = patch
  end
end

class Patch
  attr_reader :patch_name

  def initialize(patch_name)
    # initialize tags for further searches
    @patch_name = patch_name
    @tags = []
  end
end

class Author
  attr_reader :email

  def initialize(email, collaboration_cathegory, profile)
    @email = email
    # NLP input
    @cathegory = collaboration_cathegory
    # NLP input
    @profile = profile
  end
end

class Content
  attr_reader :raw_content, :keywords

  def initialize(content, keywords)
    @raw_content = content
    @keywords = keywords
  end
end
