CREATE SEQUENCE IF NOT EXISTS emails_email_id_seq START 1;

CREATE TABLE IF NOT EXISTS emails (
	email_id	BIGINT PRIMARY KEY DEFAULT nextval('emails_email_id_seq'),
	sender		VARCHAR(64) NOT NULL,
	recipient	VARCHAR(64),
	date		TIMESTAMP NOT NULL,
	message		TEXT NOT NULL,
	subject		VARCHAR(200),
	message_id	VARCHAR(100) NOT NULL,
	in_reply_to	VARCHAR(100),
	action		VARCHAR(30),
	subsystem	VARCHAR(50),
	subject_original	VARCHAR(200));

CREATE UNIQUE INDEX IF NOT EXISTS message_id_idx ON emails(message_id);

CREATE SEQUENCE IF NOT EXISTS lists_list_id_seq START 1;

CREATE TABLE IF NOT EXISTS lists (
        list_id                 INT PRIMARY KEY DEFAULT nextval('lists_list_id_seq'),
        name                    VARCHAR(40) NOT NULL,
        mail_to_subscribe       VARCHAR(64) DEFAULT 'Majordomo@vger.kernel.org',
        recipient               VARCHAR(64),
        subscribed              BOOLEAN DEFAULT false,
        unsubscribe             BOOLEAN DEFAULT false);

CREATE UNIQUE INDEX IF NOT EXISTS list_name_idx ON lists(name);

