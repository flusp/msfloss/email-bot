require 'pg'

# Gerencia o banco de dados dos emails.
# Insere uma lista de emails, de um objeto da classe Email, no banco de dados
class MailRepository
  def initialize(mail)
    @mail = mail
  end

  def opendb
    @con = PG.connect
    @con
  end

  def closedb
    @con&.close
  end

  def verify_unsubscription
    opendb
    if @con
      result = @con.exec 'SELECT * FROM lists WHERE subscribed = true '\
                         'AND unsubscribe = true;'
      result.each do |res|
        list_name = res['name']
        mail_to = res['mail_to_subscribe']
        @mail.unsubscribe(list_name, mail_to)
        @con.exec 'UPDATE lists SET subscribed = false WHERE '\
                  "name = '#{list_name}' AND unsubscribe = true;"
      end
      closedb
    end
  end

  def verify_subscription
    opendb
    if @con
      result = @con.exec 'SELECT * FROM lists WHERE subscribed = false '\
                         'AND unsubscribe = false;'
      result.each do |res|
        list_name = res['name']
        mail_to = res['mail_to_subscribe']
        @mail.add_subscribe_email(mail_to)
        @mail.subscribe(list_name, mail_to)
        @con.exec 'UPDATE lists SET subscribed = true WHERE '\
                  "name = '#{list_name}';"
      end
      closedb
    end
  end

  def insert_maillist(maillist)
    result = @con.exec "SELECT * FROM lists WHERE '#{maillist}' LIKE "\
                       "'%' || name || '%';"
    result.each do |res|
      list_name = res['name']
      list_mail = res['recipient']
      if !list_mail || (list_mail == '')
        @con.exec "UPDATE lists SET recipient = '#{maillist}' WHERE "\
                  "name = '#{list_name}';"
      end
    end
  end

  def process_subscription(msg)
    if msg.subject.full_name =~ /\s*Confirmation for subscribe.*/i
      auth = msg.content.raw_content.to_s.slice(/auth.*\n/i).sub(/<.*>/, '')
      @mail.send(msg.author.email, auth)
    elsif msg.subject.full_name =~ /\s*Welcome to.*/i
      list_name = msg.subject.full_name.sub(/Welcome to.*/i).strip
      @con.exec 'UPDATE lists SET subscribed = true, unsubscribe = false '\
                "WHERE name = '#{list_name}';"
    end
  end

  def add_errors(id, err)
    @db_errors = [id, err]
  end

  def print_errors
    @db_errors.each do |e|
      puts e
    end
  end

  def insert(msgs)
    opendb
    return unless @con

    msgs.each do |msg|
      if msg.maillist
        date = msg.date
        sender = msg.author.email
        subject = msg.subject.full_name.gsub("'", "''")
        subject_original = subject.sub(/Re:\s*/i, '')
        recipient = msg.maillist
        message_id = msg.message_id
        in_reply_to = msg.in_reply_to
        patch = msg.subject.patch
        subsystem = msg.subject.subsystem
        body = msg.content.raw_content.to_s.gsub("'", "''")
        @con.exec 'INSERT INTO emails (date,sender,recipient,subject,'\
                  'subject_original,message_id,in_reply_to,action,'\
                  "subsystem,message) VALUES('#{date}','#{sender}',"\
                  "'#{recipient}','#{subject}','#{subject_original}',"\
                  "'#{message_id}','#{in_reply_to}','#{patch}',"\
                  "'#{subsystem}','#{body}') ON CONFLICT (message_id)"\
                  ' DO NOTHING;'
        insert_maillist(recipient)
      elsif @mail.signed_of.find { |e| e.downcase == msg.author.email.downcase }
        process_subscription(msg)
      end
    rescue PG::Error => e
      add_errors(msg.message_id, e)
    end
    closedb
  end

  def remove(email_id)
    opendb
    if @con
      @con&.exec "DELETE FROM emails WHERE email_id = #{email_id};"
      closedb
    end
  end
end
